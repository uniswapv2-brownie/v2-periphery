# Uniswap V2

[![Actions Status](https://github.com/Uniswap/uniswap-v2-periphery/workflows/CI/badge.svg)](https://github.com/Uniswap/uniswap-v2-periphery/actions)
[![npm](https://img.shields.io/npm/v/@uniswap/v2-periphery?style=flat-square)](https://npmjs.com/package/@uniswap/v2-periphery)

In-depth documentation on Uniswap V2 is available at [uniswap.org](https://uniswap.org/docs).

The built contract artifacts can be browsed via [unpkg.com](https://unpkg.com/browse/@uniswap/v2-periphery@latest/).

# Local Development

The following assumes the use of `node@>=10`.

## Install Dependencies

`yarn`

## Compile Contracts

`yarn compile`

## Run Tests

`yarn test`

# Brownie environment
1. Remove all solc >0.6.6
2. Create empty folder:  
```bash
mkdir uniswap
cd uniswap
```
2. Clone Repos: 
https://github.com/Uniswap/v2-core  
https://github.com/Uniswap/v2-periphery  	
https://github.com/Uniswap/solidity-lib  

3. Compile `solidity-lib`, `v2-core`  
4. Create `uniswap` folder in standard  Brownie package folder. For 
linux it is `~/.brownie/packages`
4. Copy `v2-core`  and `solidity-lib` into  `~/.brownie/packages`  
5. Ready for run tests from `v2-periphery`
