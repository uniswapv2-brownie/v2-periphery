import pytest
#from brownie import chain
zero_address = '0x0000000000000000000000000000000000000000'

############ Mocks ########################
@pytest.fixture(scope="module")
def dai(accounts, ERC20):
    dai = accounts[0].deploy(ERC20, 100000000000e18)
    yield dai

@pytest.fixture(scope="module")
def usdr(accounts, ERC20):
    u = accounts[0].deploy(ERC20, 100000000000e18,)
    yield u    

@pytest.fixture(scope="module")
def weth(accounts, WETH9):
    w = accounts[0].deploy(WETH9)
    yield w        

@pytest.fixture(scope="module")
def factory(accounts, pm):
    fa = pm('uniswap/v2-core').UniswapV2Factory
    f = accounts[0].deploy(fa, zero_address)
    yield f    

@pytest.fixture(scope="module")
def RudaV2Pair(accounts, pm):
    fa = pm('uniswap/v2-core').UniswapV2Pair
    #f = accounts[0].deploy(fa, accounts[0])
    yield fa    

@pytest.fixture(scope="module")
def router(accounts, factory, weth, UniswapV2Router02):
    f = accounts[0].deploy(UniswapV2Router02, factory.address, weth.address)
    yield f  

      

# @pytest.fixture(scope="module")
# def weth(accounts, TokenMock):
#     weth = accounts[0].deploy(TokenMock,"WETH MOCK Token", "WETH")
#     yield weth

