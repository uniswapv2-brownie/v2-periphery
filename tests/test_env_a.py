import pytest
import logging
from brownie import chain, Wei, reverts
LOGGER = logging.getLogger(__name__)
from web3 import Web3

zero_address = '0x0000000000000000000000000000000000000000'
DEPOSIT_AMOUNT=1e18
DAIAmount = 1000e18
USDRAmount = 64000e18
DAIInAmoint = 10e18
#transfer with fee without royalty
def test_erc20_mock(accounts, usdr, dai):
    assert usdr.totalSupply() ==  dai.totalSupply() 

def test_weth(accounts, weth):
    weth.deposit({'from': accounts[1], 'value': DEPOSIT_AMOUNT})
    assert weth.balanceOf(accounts[1]) ==  DEPOSIT_AMOUNT 

def test_factory(accounts, factory):
    logging.info('factory at address: {}'.format(factory.address))
    assert factory.allPairsLength() == 0  

def test_router(accounts, factory, router):
    logging.info('router at address: {}'.format(router.address))
    assert factory.address == router.factory() 

def test_add_liquidity_with_creat(accounts, factory, router, usdr, dai, RudaV2Pair):

    assert factory.getPair(usdr, dai) == zero_address
    assert factory.getPair(dai, usdr) == zero_address

    usdr.transfer(accounts[1],USDRAmount * 3,{'from':accounts[0]})
    dai.transfer(accounts[1],DAIAmount * 3,{'from':accounts[0]})
    usdr.transfer(accounts[3],USDRAmount * 3,{'from':accounts[0]})
    dai.transfer(accounts[3],DAIAmount * 3,{'from':accounts[0]})
  
    usdr.approve(router, USDRAmount * 3,{'from':accounts[1]})
    dai.approve(router, DAIAmount * 3,{'from':accounts[1]})
    usdr.approve(router, USDRAmount * 3,{'from':accounts[3]})
    dai.approve(router, DAIAmount * 3,{'from':accounts[3]})

    logging.info('Account[1] dai balance:{}, allowance: {}'.format(
        dai.balanceOf(accounts[1]),
        dai.allowance(accounts[1], router)
    ))
    #factory.setFeeTo(accounts[2], {'from':accounts[0]})
    # tx = factory.createPair(dai, usdr, {'from':accounts[0]})
    # receipt = chain.get_transaction(tx.txid)
    # logging.info('Returned address:{}'.format(receipt.return_value))
    pair_addr  = factory.getPair(usdr, dai)
    logging.info('Calculated Pair address: {}'.format(pair_addr)) 

    router.addLiquidity(
        dai,
        usdr,
        DAIAmount,
        USDRAmount,
        0,
        0,
        accounts[1],
        chain.time() + 1000,
        {'from':accounts[1]}
    )
    